function y = proximal_operator(Xi, q, chi)
    switch q
        case 1
               y = (Xi./abs(Xi)).*max([abs(Xi) - chi; zeros(1, length(Xi))], [], 1)
        case 4/3
               epsilon = sqrt(Xi.^2 + 256 * chi^3/729)
               y = Xi + 4*chi/(3*2^(1/3))*((epsilon - Xi).^(1/3) - (epsilon + Xi).^(1/3));
        case 3/2
            y = Xi + 9*chi^2*sign(Xi)/8.*(1 - sqrt(1+16*abs(Xi)/(9*chi^2)));
        case 2
            y = Xi/(1+2*chi)
        case 3
            y = sign(Xi).*(sqrt(1+12*chi*abs(Xi)) - 1)/(6*chi);
        case 4
            epsilon = sqrt(Xi.^2 + 1/(27*chi));
            y = ((epsilon + Xi)/(8*chi)).^(1/3) - ((epsilon - Xi)/(8*chi)).^(1/3);
end